package com.shopping.shoppinglist.data.db.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "shopping_items")
data class ShoppingItem(
    @ColumnInfo(name = "item_name")
    var name: String,
    @ColumnInfo(name = "item_data")
    var date: Int,
    @ColumnInfo(name = "item_checked")
    var isChecked: Boolean = false

) {
    @PrimaryKey(autoGenerate = true)
    var id: Int? = null
}