package com.shopping.shoppinglist

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.shopping.shoppinglist.ui.shoppinglist.ShoppingActivity

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        val intent = Intent(this, ShoppingActivity::class.java)
        startActivity(intent)
        finish()

    }
}