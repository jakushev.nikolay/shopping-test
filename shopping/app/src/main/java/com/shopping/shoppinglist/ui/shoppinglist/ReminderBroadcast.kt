package com.shopping.shoppinglist.ui.shoppinglist

import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.shopping.shoppinglist.R

class ReminderBroadcast : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {

        val repeatingIntent = Intent(context, ShoppingActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        val pendingIntent = PendingIntent.getActivity(context,0, repeatingIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        var builder = NotificationCompat.Builder(context,"Notification" )
            .setContentIntent(pendingIntent)
            .setSmallIcon(R.drawable.shopping)
            .setContentTitle("Shopping!")
            .setContentText("Пора за покупками!")
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setAutoCancel(true)

        val manager = NotificationManagerCompat.from(context)

        manager.notify(200, builder.build())
    }
}