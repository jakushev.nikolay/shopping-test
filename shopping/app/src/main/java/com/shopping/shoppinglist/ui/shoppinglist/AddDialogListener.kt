package com.shopping.shoppinglist.ui.shoppinglist

import com.shopping.shoppinglist.data.db.entities.ShoppingItem

interface AddDialogListener {
    fun onAddButtonClicked(item: ShoppingItem)
}